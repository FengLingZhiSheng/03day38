package com.zhuobin.dao;

import java.util.List;
import javax.annotation.Resource;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.zhuobin.bean.EmployeeBean;
@Repository
public class EmployeeDao{
	@Resource
	private SessionFactory sf;
	public void save(EmployeeBean employee){
		sf.getCurrentSession().save(employee);
	}
	public void update(EmployeeBean employee){
		sf.getCurrentSession().update(employee);;
	}
	public void delete(EmployeeBean employee){
		sf.getCurrentSession().delete(employee);;
	}
	public EmployeeBean findById(int e_id){
		return sf.getCurrentSession().get(EmployeeBean.class, e_id);
	}
	public List<EmployeeBean> findAll(){
		return sf.getCurrentSession().createQuery("from EmployeeBean",EmployeeBean.class).getResultList();
	}
}
