package com.zhuobin.dao;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.zhuobin.bean.ZhiYiBean;
@Repository
public class ZhiyeDao{
	@Resource
	private SessionFactory sf;
	public List<ZhiYiBean> findAll(){
		return sf.getCurrentSession().createQuery("from ZhiYiBean",ZhiYiBean.class).getResultList();
	}
	public ZhiYiBean findById(int zy_id){
		return sf.getCurrentSession().get(ZhiYiBean.class, zy_id);
	}
}
