package com.zhuobin.bean;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="zhiye")
public class ZhiYiBean {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="zy_id")
	private int zy_id;
	@Column(name="zy_name")
	private String zy_name;
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=EmployeeBean.class,mappedBy="zhiye",orphanRemoval=true)
	private Set employee = new HashSet();
	public int getZy_id() {
		return zy_id;
	}
	public void setZy_id(int zy_id) {
		this.zy_id = zy_id;
	}
	public String getZy_name() {
		return zy_name;
	}
	public void setZy_name(String zy_name){
		this.zy_name = zy_name;
	}
	
}
