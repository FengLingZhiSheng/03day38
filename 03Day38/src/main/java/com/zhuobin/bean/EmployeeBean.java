package com.zhuobin.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="employee")
public class EmployeeBean {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="e_id")
	private int e_id;
	@Column(name="e_name")
	private String e_name;
	@Column(name="e_age")
	private int e_age;
	@Column(name="e_sex ")
	private String e_sex;
	@JoinColumn(name="zy_id")
	//optional当前属性是否为可选
	@ManyToOne(fetch=FetchType.EAGER,optional=true,targetEntity=ZhiYiBean.class)
	private ZhiYiBean zhiye;
	//	private ZhiYiBean zhiyi;
	public int getE_id() {
		return e_id;
	}
	public void setE_id(int e_id) {
		this.e_id = e_id;
	}
	public String getE_name(){
		return e_name;
	}
	public void setE_name(String e_name) {
		this.e_name = e_name;
	}
	public int getE_age() {
		return e_age;
	}
	public void setE_age(int e_age) {
		this.e_age = e_age;
	}
	public String getE_sex() {
		return e_sex;
	}
	public void setE_sex(String e_sex){
		this.e_sex = e_sex;
	}
	public ZhiYiBean getZhiye() {
		return zhiye;
	}
	public void setZhiye(ZhiYiBean zhiye) {
		this.zhiye = zhiye;
	}
	
}
