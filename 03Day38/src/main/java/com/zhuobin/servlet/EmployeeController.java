package com.zhuobin.servlet;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zhuobin.bean.EmployeeBean;
import com.zhuobin.bean.ZhiYiBean;
import com.zhuobin.biz.EmployeeBiz;
import com.zhuobin.biz.ZhiyeBiz;

//@Component 表示一个普通组件
//@Controller 表示一个控制器组件
//@Service 表示一个业务组件（biz）
//@Repository 表示一个DAO组件
@Controller
//@RequestMapping("/employee")//表示访问的根路径
public class EmployeeController {
	@Resource
	private EmployeeBiz biz;
	@Resource
	private ZhiyeBiz zhiyeBiz;
	@RequestMapping(value="/employees",method=RequestMethod.GET)
	// 方法可以返回String，表示跳转的路径，会加上配置文件中的前缀和后缀
	// 方法也可以直接返回void，那么会自动生成返回路径为根路径+方法路径+前缀和后缀的方式，例如本方法将跳转到/WEB-INF/book/list.jsp
	public String list(Map<String,Object> request){
		System.out.println(111111111);
		List<EmployeeBean> list = biz.findAll();
		request.put("list",list);
		return "employee/list";
	}
	@RequestMapping(value="/employee",method=RequestMethod.GET)
	public String preAdd(Map<String, Object> request){
		List<ZhiYiBean> list=zhiyeBiz.findAll();
		request.put("list", list);
		return "employee/add";
	}
	@RequestMapping(value="/employees",method=RequestMethod.POST)
	public String doAdd(EmployeeBean employee){
		biz.save(employee);
		return "redirect:/employees";
	}
	@RequestMapping(value="/employees/{e_id}",method=RequestMethod.GET)
	public String preUpdate(Map<String, Object> request,@PathVariable("e_id")int e_id){
		EmployeeBean emp = biz.findById(e_id); 
		request.put("emp", emp);
		List<ZhiYiBean> zhiye=zhiyeBiz.findAll();
		request.put("zhiye", zhiye);
		return "employee/update";
	}
	@RequestMapping(value="employees",method=RequestMethod.PUT)
	public String doUpdate(EmployeeBean employee){
		employee.setZhiye(zhiyeBiz.findById(employee.getZhiye().getZy_id()));
		biz.update(employee);
		return "redirect:/employees";
	}
	@RequestMapping(value="employees/{e_id}",method=RequestMethod.DELETE)
	public String delete(EmployeeBean employee){
		biz.delete(employee);
		return "redirect:/employees";
	}
}
