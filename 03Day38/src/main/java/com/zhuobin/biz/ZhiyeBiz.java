package com.zhuobin.biz;
import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.zhuobin.bean.ZhiYiBean;
import com.zhuobin.dao.ZhiyeDao;
@Service
public class ZhiyeBiz {
	@Resource
	private ZhiyeDao zyDao;
	public List<ZhiYiBean> findAll(){
		return zyDao.findAll();
	}
	public ZhiYiBean findById(int zy_id){
		return zyDao.findById(zy_id);
	}
}