package com.zhuobin.biz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zhuobin.bean.EmployeeBean;
import com.zhuobin.dao.EmployeeDao;
@Service
public class EmployeeBiz {
	@Resource
	private EmployeeDao empDao;
	public List<EmployeeBean> findAll(){
		return empDao.findAll();
	}
	public EmployeeBean findById(int e_id){
		return empDao.findById(e_id);
	}
	public void save(EmployeeBean employee){
		empDao.save(employee);
	}
	public void update(EmployeeBean employee){
		empDao.update(employee);
	}
	public void delete(EmployeeBean employee){
		empDao.delete(employee);
	}
}
